﻿using UnityEngine;

public abstract class InputControllerBase : MonoBehaviour
{
	public abstract float GetHorizontalAxis();

	public abstract float GetVerticalAxis();

	public abstract float GetRotation();

	public abstract bool GetSimpleAttack();

	public abstract bool GetStrongAttack();
}