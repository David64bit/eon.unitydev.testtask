﻿using UnityEngine;
using JetBrains.Annotations;

public class PCInputController : InputControllerBase
{
	[SerializeField, UsedImplicitly]
	private string _horizontalAxisName = "Horizontal";

	[SerializeField, UsedImplicitly]
	private string _verticalAxisName = "Vertical";

	[SerializeField, UsedImplicitly]
	private string _mouseAxisName = "Mouse X";

	public override float GetHorizontalAxis()
	{
		return Input.GetAxis(_horizontalAxisName);
	}

	public override float GetVerticalAxis()
	{
		return Input.GetAxis(_verticalAxisName);
	}

	public override float GetRotation()
	{
		return Input.GetAxis(_mouseAxisName);
	}

	public override bool GetSimpleAttack()
	{
		return Input.GetMouseButtonDown(0);
	}

	public override bool GetStrongAttack()
	{
		return Input.GetMouseButtonDown(1);
	}
}