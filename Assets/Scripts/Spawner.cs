﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
	[SerializeField]
	private Vector3 _minSpawnPoint;

	[SerializeField]
	private Vector3 _maxSpawnPoint;

	public T Spawn<T>(T prefab) where T : MonoBehaviour
	{
		Vector3 position = new Vector3(
			Random.Range(_minSpawnPoint.x, _maxSpawnPoint.x),
			Random.Range(_minSpawnPoint.y, _maxSpawnPoint.y),
			Random.Range(_maxSpawnPoint.z, _minSpawnPoint.z));

		var instance = Instantiate(prefab, position, Quaternion.identity);

		return instance;
	}
}