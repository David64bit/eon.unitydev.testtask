﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	[Serializable]
	public class InteractiveIconPair
	{
		public InteractiveType Type;

		public Sprite Icon;
	}

	[SerializeField]
	private List<InteractiveIconPair> _interactiveIconPairs;

	[SerializeField]
	private GameObject _iconsHolder;

	[SerializeField]
	private Image _iconPrefab;

	public void Init(List<InteractiveType> interactiveTypes)
	{
		foreach (InteractiveType type in interactiveTypes)
		{
			Image iconInstance = Instantiate(_iconPrefab, _iconsHolder.transform);

			InteractiveIconPair pair = _interactiveIconPairs.Find(p => p.Type == type);

			if (pair == null)
			{
				Debug.LogAssertion($"Icon for {type} not found");
				continue;
			}

			iconInstance.sprite = pair.Icon;
		}
	}

	public void DeInit()
	{
		for (int i = 0; i < _iconsHolder.transform.childCount; ++i)
		{
			Destroy(_iconsHolder.transform.GetChild(i).gameObject);
		}
	}
}