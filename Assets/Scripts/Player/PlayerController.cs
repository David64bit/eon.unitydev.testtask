﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
	private readonly int SpeedAnimatorID = Animator.StringToHash("Speed");
	private readonly int SpeedMultiplierAnimatorID = Animator.StringToHash("SpeedMultiplier");
	private readonly int HorizontalAnimatorID = Animator.StringToHash("Horizontal");
	private readonly int VerticalAnimatorID = Animator.StringToHash("Vertical");
	private readonly int SimpleAttack1ID = Animator.StringToHash("SimpleAttack1");
	private readonly int SimpleAttack2ID = Animator.StringToHash("SimpleAttack2");
	private readonly int StrongAttackID = Animator.StringToHash("StrongAttack");

	[Header("References")]
	[SerializeField]
	private InputControllerBase _inputController;

	[SerializeField]
	private BoxCollider _weaponCollider;

	[SerializeField]
	private Animator _animator;

	[Header("Settings")]
	[SerializeField]
	[Range(1f, 2f)]
	private float _maxSpeed;

	public float Speed { get; private set; }

	private float SpeedMultiplier { get; set; }

	public void ManualFixedUpdate()
	{
		UpdatePosition();

		UpdateRotation();

		UpdateAttack();
	}

	private void UpdatePosition()
	{
		float xDelta = _inputController.GetHorizontalAxis();
		float yDelta = _inputController.GetVerticalAxis();

		_animator.SetFloat(HorizontalAnimatorID, xDelta);
		_animator.SetFloat(VerticalAnimatorID, yDelta);

		Speed = Mathf.Max(Mathf.Abs(xDelta) + Mathf.Abs(yDelta));
		_animator.SetFloat(SpeedAnimatorID, Speed);

		SpeedMultiplier = 1f + (_maxSpeed - 1f) * Speed; 
		_animator.SetFloat(SpeedMultiplierAnimatorID, SpeedMultiplier);
	}

	private void UpdateRotation()
	{
		float delta = _inputController.GetRotation();

		transform.rotation *= Quaternion.AngleAxis(delta, Vector3.up);
	}

	public void UpdateAttack()
	{
		if (_inputController.GetSimpleAttack())
		{
			_animator.ResetTrigger(SimpleAttack1ID);
			_animator.ResetTrigger(SimpleAttack2ID);

			int attackVariation = Random.Range(1, 3);

			int animationID = attackVariation == 1 ? SimpleAttack1ID : SimpleAttack2ID;

			_animator.SetTrigger(animationID);
		}
		else if (_inputController.GetStrongAttack())
		{
			_animator.ResetTrigger(SimpleAttack1ID);
			_animator.ResetTrigger(SimpleAttack2ID);

			_animator.SetTrigger(StrongAttackID);
		}
	}

	private void Hit()
	{
		var hits = Physics.BoxCastAll(_weaponCollider.transform.TransformPoint(_weaponCollider.center),
									  _weaponCollider.transform.TransformVector(_weaponCollider.size) / 2f,
									  _weaponCollider.transform.forward,
									  _weaponCollider.transform.rotation);

		foreach (RaycastHit hit in hits)
		{
			hit.collider.GetComponent<IHitable>()?.TakeHit();
		}
	}
}