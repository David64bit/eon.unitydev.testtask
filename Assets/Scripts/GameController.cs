﻿using UnityEngine;
using JetBrains.Annotations;
using System.Collections;

public class GameController : MonoBehaviour
{
	[Header("Prefabs")]

	[SerializeField]
	private PlayerController _playerControllerPrefab;

	[Header("References")]

	[SerializeField]
	private Spawner _spawner;

	[SerializeField]
	private InteractiveController _interactiveController;

	[SerializeField]
	private UIController _uiController;

	private PlayerController _playerController;

	[UsedImplicitly]
	private void Start()
	{
		Init();
	}

	private void Init()
	{
		_interactiveController.Init();
		_interactiveController.EventOnSequnceComplete += OnSequenceCompleted;

		_uiController.Init(_interactiveController.Sequence);

		_playerController = _spawner.Spawn(_playerControllerPrefab);
	}

	private void DeInit()
	{
		_interactiveController.DeInit();
		_interactiveController.EventOnSequnceComplete -= OnSequenceCompleted;

		_uiController.DeInit();

		Destroy(_playerController.gameObject);
	}

	private void OnSequenceCompleted()
	{
		StartCoroutine(RestartLevel());
	}

	private IEnumerator RestartLevel()
	{
		const float delayBeforeRestart = 3f;

		Debug.Log($"Victory! Level will be restared in {delayBeforeRestart} seconds.");

		yield return new WaitForSeconds(delayBeforeRestart);

		DeInit();

		Init();
	}

	[UsedImplicitly]
	private void FixedUpdate()
	{
		_playerController.ManualFixedUpdate();
	}
}
