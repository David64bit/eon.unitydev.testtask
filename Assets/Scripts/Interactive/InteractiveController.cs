﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

public class InteractiveController : MonoBehaviour
{
	public event Action EventOnSequnceComplete;

	[SerializeField, UsedImplicitly]
	private List<InteractiveBehaviour> _interactivePrefabs;

	[SerializeField, UsedImplicitly]
	private Spawner _spawner;

	private static IEnumerable<InteractiveType> InteractiveTypes { get; } = Enum.GetValues(typeof(InteractiveType)).Cast<InteractiveType>();

	public List<InteractiveType> Sequence { get; private set; }

	private IEnumerator<InteractiveType> SequenceEnumetor { get; set; }

	private List<InteractiveBehaviour> InteractiveBehaviours { get; set; } = new List<InteractiveBehaviour>();

	private Stack<InteractiveBehaviour> ActivatedBehaviours { get; } = new Stack<InteractiveBehaviour>();

	private Stack<InteractiveBehaviour> DeactivatedBehaviours { get; } = new Stack<InteractiveBehaviour>();

	public void Init()
	{
		SpawnObejcts();

		GenerateSequence(); 
	}

	public void DeInit()
	{
		foreach (InteractiveBehaviour behaviour in InteractiveBehaviours)
		{
			Destroy(behaviour.gameObject);
		}

		InteractiveBehaviours.Clear();
		ActivatedBehaviours.Clear();
		DeactivatedBehaviours.Clear();

		Sequence.Clear();
		SequenceEnumetor = null;
	}

	private void SpawnObejcts()
	{
		foreach (InteractiveType interactiveType in InteractiveTypes)
		{
			InteractiveBehaviour prefab = _interactivePrefabs.Find(behaviour => behaviour.Type == interactiveType);
			if (prefab == null)
			{
				Debug.LogAssertion($"Prefab for object {interactiveType} is missed.");
				continue;
			}

			InteractiveBehaviour instance = _spawner.Spawn(prefab);

			InteractiveBehaviours.Add(instance);

			instance.EventOnIntect += OnObjectInteracted;
		}
	}

	private void GenerateSequence()
	{
		Sequence = InteractiveTypes.OrderBy(type => Random.Range(0f, 100f)).ToList();

		SequenceEnumetor = Sequence.GetEnumerator();
		SequenceEnumetor.MoveNext();

		Debug.Log("Sequence generated.");
		foreach(InteractiveType type in Sequence)
		{
			Debug.Log(type);
		}
	}

	private void OnObjectInteracted(InteractiveBehaviour interactiveBehaviour)
	{
		if (interactiveBehaviour.Type != SequenceEnumetor.Current)
		{
			Debug.Log($"Failure! {interactiveBehaviour.Type} activated instead of {SequenceEnumetor.Current}");

			interactiveBehaviour.Activated = false;

			DeactivatedBehaviours.Push(interactiveBehaviour);

			while (ActivatedBehaviours.Count > 0)
			{
				ActivatedBehaviours.Pop().Activated = null;
			}

			SequenceEnumetor = Sequence.GetEnumerator();
			SequenceEnumetor.MoveNext();
		}
		else
		{
			Debug.Log($"Success! {interactiveBehaviour.Type} activated.");

			interactiveBehaviour.Activated = true;

			ActivatedBehaviours.Push(interactiveBehaviour);

			while (DeactivatedBehaviours.Count > 0)
			{
				DeactivatedBehaviours.Pop().Activated = null;
			}

			if (!SequenceEnumetor.MoveNext())
			{
				EventOnSequnceComplete?.Invoke();
			}
		}
	}
}
