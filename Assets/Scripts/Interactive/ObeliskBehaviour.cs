﻿using System;
using UnityEngine;
using JetBrains.Annotations;

public class ObeliskBehaviour : InteractiveBehaviour
{
	public override InteractiveType Type => InteractiveType.Obelisk;

	private float PreviousAngle { get; set; }

	private float CurrentAngle { get; set; }

	private float PreviousDelta { get; set; }

	private float CurrentDelta { get; set; }

	private float SumAngle { get; set; }

	[UsedImplicitly]
	private void OnTriggerEnter(Collider collider)
	{
		if (Activated.HasValue)
		{
			return;
		}

		PlayerController playerController = collider.GetComponent<PlayerController>();

		if (playerController == null)
		{
			return;
		}

		CurrentAngle = GetAngleTo(playerController.transform);

		SumAngle = 0f;
	}

	private float GetAngleTo(Transform target)
	{
		Vector3 currentDirectionToTarget = (target.position - transform.position).normalized;
		return Mathf.Atan2(-currentDirectionToTarget.x, currentDirectionToTarget.z) * Mathf.Rad2Deg + 180f;
	}

	[UsedImplicitly]
	private void OnTriggerStay(Collider collider)
	{
		if (Activated.HasValue)
		{
			return;
		}

		PlayerController playerController = collider.GetComponent<PlayerController>();

		if (playerController == null)
		{
			return;
		}

		PreviousAngle = CurrentAngle;

		CurrentAngle = GetAngleTo(playerController.transform);

		PreviousDelta = CurrentDelta;

		CurrentDelta = Mathf.DeltaAngle(PreviousAngle, CurrentAngle);

		if (Mathf.Sign(PreviousDelta) != Mathf.Sign(CurrentDelta))
		{
			SumAngle = 0;
		}

		SumAngle += CurrentDelta;

		float absSumAngle = Mathf.Abs(SumAngle);

		if (absSumAngle >= 360f)
		{
			Interact();
		}
	}
}