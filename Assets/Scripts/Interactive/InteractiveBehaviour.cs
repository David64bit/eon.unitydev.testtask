﻿using UnityEngine;
using System;

public abstract class InteractiveBehaviour : MonoBehaviour
{
	public event Action<InteractiveBehaviour> EventOnIntect;

	[SerializeField]
	private ParticleSystem _succesParticle;

	[SerializeField]
	private ParticleSystem _failureParticle;

	public abstract InteractiveType Type { get; }

	private bool? _activated;
	
	public bool? Activated
	{
		get => _activated;
		set
		{
			_activated = value;

			if (_activated.HasValue)
			{
				if (_activated.Value)
				{
					_succesParticle.Play();
					_failureParticle.Stop();
				}
				else
				{
					_succesParticle.Stop();
					_failureParticle.Play();
				}
			}
			else
			{
				_succesParticle.Stop();
				_failureParticle.Stop();
			}
		}
	}

	protected void Interact()
	{
		EventOnIntect?.Invoke(this);
	}
}