﻿using UnityEngine;
using JetBrains.Annotations;

public class SquareBehaviour : InteractiveBehaviour
{
	public override InteractiveType Type => InteractiveType.Square;

	[UsedImplicitly]
	private void OnTriggerStay(Collider collider)
	{
		if (Activated.HasValue)
		{
			return;
		}

		PlayerController playerController = collider.GetComponent<PlayerController>();

		if (playerController == null)
		{
			return;
		}

		if (playerController.Speed > 0)
		{
			return;
		}

		Interact();
	}
}