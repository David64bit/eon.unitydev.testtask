﻿public class VaseBehaviour : InteractiveBehaviour, IHitable
{
	public override InteractiveType Type => InteractiveType.Vase;

	public void TakeHit()
	{
		if (Activated.HasValue && Activated.Value)
		{
			return;
		}

		Interact();
	}
}